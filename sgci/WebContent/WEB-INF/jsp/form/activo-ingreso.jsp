<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container-fluid">
	<div class="row"></div>
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div class="card card-nav-tabs">
				<div class="card-header" data-background-color="red">
					<div class="nav-tabs-navigation">
						<div class="nav-tabs-wrapper">
							<ul class="nav nav-tabs" data-tabs="tabs">
								<li class="active"><a id="activo" data-toggle="tab"> <i class="material-icons">receipt</i> Ingreso de Activos
										<div class="ripple-container"></div></a></li>
								<li class=""><a id="metrica" data-toggle="tab"> <i class="material-icons">code</i> M�trica
										<div class="ripple-container"></div></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div id="content-activo" class="col-md-12">
					<div class="tab-content">
						<div class="tab-pane active" id="activo">
							<div class="card">
								<div class="card-content">
									<h4 class="title">ACTIVO</h4>
									<form action="/sgci/api/grabarActivo" id="formularioGrabar" method="post">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Nombre</label> <input type="text" class="form-control" name="nombre" id="nombre" required="required">
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">Descripci�n</label> <input type="text" class="form-control" name="descripcion" id="descripcion" required="required">
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
												<div class="">
													<div class="form-group label-floating dropdown-toggle" data-toggle="dropdown">
														<label class="control-label">Propietario</label> <input type="text" class="form-control" name="propietario" id="propietario" required="required">
													</div>
													<ul class="dropdown-menu">
														<li><a class="propietario" href="javascript:void(0)" data-name="Jorge Sanchez">Jorge Sanchez</a></li>
														<li><a class="propietario" href="javascript:void(0)" data-name="Steven Navarro">Steven Navarro</a></li>
														<li><a class="propietario" href="javascript:void(0)" data-name="Sergio Vargas">Sergio Vargas</a></li>
														<li><a class="propietario" href="javascript:void(0)" data-name="Luis Padilla">Luis Padilla</a></li>
													</ul>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
												<div class="form-group label-floating">
													<label class="control-label">�rea</label> <input type="text" class="form-control" name="area" id="area" required="required">
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-12">
												<div class="dropdown">
													<div class="form-group label-floating dropdown-toggle" data-toggle="dropdown">
														<label class="control-label">Tipo</label> <input type="text" class="form-control" name="tipo" id="tipo" required="required">
													</div>
													<ul class="dropdown-menu">
														<li><a class="tipo" href="javascript:void(0)" data-name="Software">Software</a></li>
														<li><a class="tipo" href="javascript:void(0)" data-name="Hardware">Hardware</a></li>
														<li><a class="tipo" href="javascript:void(0)" data-name="Informacion">Informacion</a></li>
														<li><a class="tipo" href="javascript:void(0)" data-name="Servicios">Servicios</a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="dropdown">
													<div class="form-group label-floating dropdown-toggle" data-toggle="dropdown">
														<label class="control-label">Categoria</label> <input type="text" class="form-control" name="categoria" id="categoria" required="required">
													</div>
													<ul class="dropdown-menu">
														<li><a class="categoria" href="javascript:void(0)" data-name="Inmueble">Inmueble</a></li>
														<li><a class="categoria" href="javascript:void(0)" data-name="Personal">Personal</a></li>
														<li><a class="categoria" href="javascript:void(0)" data-name="Equipo">Equipo</a></li>
														<li><a class="categoria" href="javascript:void(0)" data-name="Aplicaciones">Aplicaciones</a></li>
														<li><a class="categoria" href="javascript:void(0)" data-name="Documentos">Documentos</a></li>
														<li><a class="categoria" href="javascript:void(0)" data-name="Datos">Datos</a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="dropdown">
													<div class="form-group label-floating dropdown-toggle" data-toggle="dropdown">
														<label class="control-label">Clasificaci�n</label> <input type="text" class="form-control" name="clasificacion" id="clasificacion" required="required">
													</div>
													<ul class="dropdown-menu">
														<li><a class="clasificacion" href="javascript:void(0)" data-name="Software">P�blico</a></li>
														<li><a class="clasificacion" href="javascript:void(0)" data-name="Hardware">Uso Interno</a></li>
														<li><a class="clasificacion" href="javascript:void(0)" data-name="Informacion">Uso Externo</a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="dropdown">
													<div class="form-group label-floating dropdown-toggle" data-toggle="dropdown">
														<label class="control-label">Confidencialidad</label> <input type="text" class="form-control" name="confidencialidad" id="confidencialidad" required="required">
													</div>
													<ul class="dropdown-menu">
														<li><a class="" href="javascript:void(0)" data-name="1">1</a></li>
														<li><a class="confidencialidad" href="javascript:void(0)" data-name="2">2</a></li>
														<li><a class="confidencialidad" href="javascript:void(0)" data-name="3">3</a></li>
														<li><a class="confidencialidad" href="javascript:void(0)" data-name="4">4</a></li>
														<li><a class="confidencialidad" href="javascript:void(0)" data-name="5">5</a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="dropdown">
													<div class="form-group label-floating dropdown-toggle" data-toggle="dropdown">
														<label class="control-label">Integridad</label> <input type="text" class="form-control" name="integridad" id="integridad" required="required">
													</div>
													<ul class="dropdown-menu">
														<li><a class="integridad" href="javascript:void(0)" data-name="1">1</a></li>
														<li><a class="integridad" href="javascript:void(0)" data-name="2">2</a></li>
														<li><a class="integridad" href="javascript:void(0)" data-name="3">3</a></li>
														<li><a class="integridad" href="javascript:void(0)" data-name="4">4</a></li>
														<li><a class="integridad" href="javascript:void(0)" data-name="5">5</a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="dropdown">
													<div class="form-group label-floating dropdown-toggle" data-toggle="dropdown">
														<label class="control-label">Disponibilidad</label> <input type="text" class="form-control" name="disponibilidad" id="disponibilidad" required="required">
													</div>
													<ul class="dropdown-menu">
														<li><a class="disponibilidad" href="javascript:void(0)" data-name="1">1</a></li>
														<li><a class="disponibilidad" href="javascript:void(0)" data-name="2">2</a></li>
														<li><a class="disponibilidad" href="javascript:void(0)" data-name="3">3</a></li>
														<li><a class="disponibilidad" href="javascript:void(0)" data-name="4">4</a></li>
														<li><a class="disponibilidad" href="javascript:void(0)" data-name="5">5</a></li>
														<li><a class="disponibilidad" href="javascript:void(0)" data-name="6">6</a></li>
													</ul>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="dropdown">
													<div class="form-group label-floating dropdown-toggle" data-toggle="dropdown">
														<label class="control-label">Valoracion</label> <input type="text" class="form-control" name="valoracion" id="valoracion" required="required">
													</div>
													<ul class="dropdown-menu">
														<li><a class="valoracion" href="javascript:void(0)" data-name="1">Alto</a></li>
														<li><a class="valoracion" href="javascript:void(0)" data-name="2">Medio</a></li>
														<li><a class="valoracion" href="javascript:void(0)" data-name="3">Bajo</a></li>
													</ul>
												</div>
											</div>
										</div>
										<button type="submit" class="btn btn-success pull-right" id="butonGrabar">Grabar</button>
										<button type="button" class="btn btn-default pull-right" id="butonCancelar">Cancelar</button>
										<div class="clearfix"></div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="content-metrica" class="col-md-12">
					<div class="tab-content">
						<div class="tab-pane active" id="metrica">
							<div class="card">
								<img src="${prop['config.url.resources.base']}/images/metrica.png" class="img-responsive" style="margin-left: auto; margin-right: auto; display: block; width: 800px;">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {

		$("#formularioGrabar").submit(function(event) {
			var data = $(this).serializeArray();
			var url = $(this).attr("action");
			$.ajax({
				url : url,
				type : "POST",
				data : data,
				success : function(data) {
					document.getElementById("formularioGrabar").reset();
					alert(data.mensaje)
				},
				error : function(jqXHR, textStatus, errorThrown) {
					//if fails      
				}
			});
			event.preventDefault();
		});

		$("#butonGrabar").on('click', function() {

			console.log($("#formularioGrabar").serialize())
		})

		$('.propietario').on("click", function() {
			$("#propietario").val($(this).data("name"))
		})

		$('.tipo').on("click", function() {
			$("#tipo").val($(this).data("name"))
		})

		$('.categoria').on("click", function() {
			$("#categoria").val($(this).data("name"))
		})

		$('.valoracion').on("click", function() {
			$("#valoracion").val($(this).data("name"))
		})

		$('.clasificacion').on("click", function() {
			$("#clasificacion").val($(this).data("name"))
		})

		$('.confidencialidad').on("click", function() {
			$("#confidencialidad").val($(this).data("name"))
		})

		$('.integridad').on("click", function() {
			$("#integridad").val($(this).data("name"))
		})

		$('.disponibilidad').on("click", function() {
			$("#disponibilidad").val($(this).data("name"))
		})
	});
</script>
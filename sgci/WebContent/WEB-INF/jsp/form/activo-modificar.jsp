<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container-fluid">
	<div class="row"></div>
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div class="card card-nav-tabs">
				<div class="card-header" data-background-color="red">
					<div class="nav-tabs-navigation">
						<div class="nav-tabs-wrapper">
							<ul class="nav nav-tabs" data-tabs="tabs">
								<li class="active"><a id="activo" data-toggle="tab"> <i class="material-icons">receipt</i>Identificaci�n de Amenzas y Vulnerabilidades
										<div class="ripple-container"></div></a></li>
								<li class=""><a id="metrica" data-toggle="tab"> <i class="material-icons">code</i> M�trica
										<div class="ripple-container"></div></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div id="content-activo" class="col-md-12">
					<div class="tab-content">
						<div class="tab-pane active" id="activo">
							<div class="col-md-12">
								<div class="col-md-6 text-center">
									<form class="navbar-form" role="search">
										<div class="form-group  is-empty">
											<input type="text" class="form-control" placeholder="Buscar por Categor�a"> <span class="material-input"></span>
										</div>
										<button type="submit" class="btn btn-white btn-round btn-just-icon">
											<i class="material-icons">search</i>
											<div class="ripple-container"></div>
										</button>
									</form>
								</div>
								<div class="col-md-6 text-center">
									<form class="navbar-form " role="search">
										<div class="form-group  is-empty">
											<input type="text" class="form-control" placeholder="Buscar por Tipo"> <span class="material-input"></span>
										</div>
										<button type="submit" class="btn btn-white btn-round btn-just-icon">
											<i class="material-icons">search</i>
											<div class="ripple-container"></div>
										</button>
									</form>
								</div>
							</div>
							<div class="card">
								<div class="card-content table-responsive">
									<h4 class="title">ACTIVO</h4>
									<table class="table" id="activosRegistrados" cellspacing="0" width="100%">
										<thead class="text-danger">
											<th>C�digo</th>
											<th>Nombre</th>
											<th>Descripcion</th>
											<th>Confidencialidad</th>
											<th>Integridad</th>
											<th>Disponibilidad</th>
											<th style="width: 207px !important;">Amenaza/Vulnerabilidad</th>
										</thead>
									</table>
									<button type="submit" class="btn btn-success pull-right">Grabar</button>
									<button type="submit" class="btn btn-default pull-right">Cancelar</button>
								</div>
							</div>

						</div>
					</div>
				</div>

				<div id="content-metrica" class="col-md-12">
					<div class="tab-content">
						<div class="tab-pane active" id="ptr">
							<div class="card">
								<img src="${prop['config.url.resources.base']}/images/metrica.png" class="img-responsive" style="margin-left: auto; margin-right: auto; display: block; width: 800px;">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var idActivo = '';
		$('#activosRegistrados').dataTable({
			bLengthChange : false,
			bProcessing : true,
			bFilter : false,
			serverSide : false,
			bRetrieve : true,
			scrollX : true,
			sAjaxSource : '/sgci/api/listarActivos',
			"columns" : [ {
				"data" : "id"
			}, {
				"data" : "nombre"
			}, {
				"data" : "descripcion"
			}, {
				"data" : "confidencialidad"
			}, {
				"data" : "integridad"
			}, {
				"data" : "disponibilidad"
			}, {
				"data" : "id",
				"render" : _BotonesAniadir,
				"bSortable" : false
			}

			],
			"order" : [ [ 0, "asc" ] ]
		});

		function _BotonesAniadir(data, type, full) {
			return '<a type="button" class="btn btn-success btn-xs agregar-button">Agregar</a>';
		}
		
		$('body').on('click','a.agregar-button',function(e) {
			idActivo = $('#activosRegistrados').DataTable().row($(this).parents('tr')).data().id;
			$('#modalAmenazaVulnerabilida').modal('show');
		});
		
		$("#formularioAmenazaVulnerablididad").submit(function(event) {
			var extra = new Object();
			var data = $(this).serializeArray();
			extra.name = 'codigoActivo';
			extra.value = idActivo;
			data.push(extra);
			var url = $(this).attr("action");
			console.log(JSON.stringify(data))
			$.ajax({
				url : url,
				type : "POST",
				data : data,
				success : function(data) {
					document.getElementById("formularioAmenazaVulnerablididad").reset();
					$('#modalAmenazaVulnerabilida').modal('hide');
					alert(data.mensaje);
				},
				error : function(jqXHR, textStatus, errorThrown) {
					//if fails      
				}
			});
			event.preventDefault();
		});
	})
</script>
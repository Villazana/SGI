<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container-fluid">
	<div class="row"></div>
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div class="card card-nav-tabs">
				<div class="card-header" data-background-color="red">
					<div class="nav-tabs-navigation">
						<div class="nav-tabs-wrapper">
							<ul class="nav nav-tabs" data-tabs="tabs">
								<li class="active"><a id="activo" data-toggle="tab"> <i class="material-icons">receipt</i>Evaluaci&oacute;n de Riesgos
										<div class="ripple-container"></div></a></li>
								<li class=""><a id="metrica" data-toggle="tab"> <i class="material-icons">code</i> Métrica
										<div class="ripple-container"></div></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div id="content-activo" class="col-md-12">
					<div class="col-md-12">
						<div class="col-md-6 text-center">
							<form class="navbar-form" role="search">
								<div class="form-group  is-empty">
									<input type="text" class="form-control" placeholder="Buscar por Categoría"> <span class="material-input"></span>
								</div>
								<button type="submit" class="btn btn-white btn-round btn-just-icon">
									<i class="material-icons">search</i>
									<div class="ripple-container"></div>
								</button>
							</form>
						</div>
						<div class="col-md-6 text-center">
							<form class="navbar-form " role="search">
								<div class="form-group  is-empty">
									<input type="text" class="form-control" placeholder="Buscar por Tipo"> <span class="material-input"></span>
								</div>
								<button type="submit" class="btn btn-white btn-round btn-just-icon">
									<i class="material-icons">search</i>
									<div class="ripple-container"></div>
								</button>
							</form>
						</div>
					</div>
					<div class="card">
						<div class="card-content table-responsive">
							<table class="table" id="listaRiesgos" cellspacing="0" width="100%">
								<thead class="text-danger">
									<th>Código</th>
									<th>Nombre</th>
									<th>Amenaza</th>
									<th>Vulnerabilidad</th>
									<th>C</th>
									<th>I</th>
									<th>D</th>
									<th>Valor CID</th>
									<th>F</th>
									<th>I</th>
									<th>Valoración</th>
									<th>Riesgo</th>
									<th>Nombre del Riesgo</th>
								</thead>
								<!--  <tbody>
									<tr>
										<td>RP01</td>
										<td>Aire acondicionado</td>
										<td>A temperatura alta, mucho calor</td>
										<td>Aire Acondicionado uniforme en el edificio</td>
										<td><select id="select_c_1" oninput="getValoracionCID(this)">
												<option value="5">5</option>
												<option value="4" selected>4</option>
												<option value="3">3</option>
												<option value="2">2</option>
												<option value="1">1</option>
										</select></td>
										<td><select id="select_i_1" oninput="getValoracionCID(this)">
												<option value="5">5</option>
												<option value="4" selected>4</option>
												<option value="3">3</option>
												<option value="2">2</option>
												<option value="1" >1</option>
										</select></td>
										<td><select id="select_d_1" oninput="getValoracionCID(this)">
												<option value="5">5</option>
												<option value="4"selected>4</option>
												<option value="3">3</option>
												<option value="2">2</option>
												<option value="1" >1</option>
										</select></td>
										<td id="cid_1">4</td>
										<td><select id="select_freq_1" oninput="getValoracion(this)">
												<option value="5">Casi Cierta</option>
												<option value="4">Muy Probable</option>
												<option value="3">Probable</option>
												<option value="2">Poco Probable</option>
												<option value="1" selected>Rara Vez</option>
										</select></td>
										<td><select id="select_impa_1" oninput="getValoracion(this)">
												<option value="1">Insignificante</option>
												<option value="2">Menor</option>
												<option value="3" selected>Moderado</option>
												<option value="4">Mayor</option>
												<option value="5">Catastrófico</option>
										</select></td>
										<td id="valoracion_1">Bajo</td>
										<td>RI-01</td>
										<td>Temperatura Alta por mala configuracion de aire acondicionado</td>
									</tr>
																		<tr>
									<td>RP16</td>
										<td>Plan de Pruebas</td>
										<td>Perdida o sustracción del documento</td>
										<td>No tenerdocumentación digital</td>
										<td><select id="select_c_31" oninput="getValoracionCID(this)">
												<option value="5">5</option>
												<option value="4">4</option>
												<option value="3"selected>3</option>
												<option value="2">2</option>
												<option value="1">1</option>
										</select></td>
										<td><select id="select_i_31" oninput="getValoracionCID(this)">
												<option value="5">5</option>
												<option value="4"  >4</option>
												<option value="3" >3</option>
												<option value="2" selected>2</option>
												<option value="1">1</option>
										</select></td>
										<td><select id="select_d_31" oninput="getValoracionCID(this)">
												<option value="5" >5</option>
												<option value="4">4</option>
												<option value="3" selected>3</option>
												<option value="2">2</option>
												<option value="1">1</option>
										</select></td>
										<td id="cid_31"></td>
										<td><select id="select_freq_31" oninput="getValoracion(this)">
												<option value="5">Casi Cierta</option>
												<option value="4">Muy Probable</option>
												<option value="3">Probable</option>
												<option value="2">Poco Probable</option>
												<option value="1" selected>Rara Vez</option>
										</select></td>
										<td><select id="select_impa_31" oninput="getValoracion(this)">
												<option value="1">Insignificante</option>
												<option value="2">Menor</option>
												<option value="3" selected>Moderado</option>
												<option value="4">Mayor</option>
												<option value="5">Catastrófico</option>
										</select></td>
										<td id="valoracion_31">Bajo</td>
										<td>RI-31</td>
										<td>Perdida de información</td>
									</tr>

								</tbody>-->
							</table>

						</div>
					</div>
				</div>

				<div id="content-metrica" class="col-md-12">
					<div class="tab-content">
						<div class="tab-pane active" id="ptr">
							<div class="card">
								<img src="${prop['config.url.resources.base']}/images/metrica.png" class="img-responsive" style="margin-left: auto; margin-right: auto; display: block; width: 800px;">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style>

.semaforo{
	background-color: black;
	margin:0 auto;
	padding: 5px;
	width:60px;
	text-align: center;
	moz-border-radius: 15px;
	-webkit-border-radius: 15px;
	border-radius: 15px;
 	overflow:auto; 
	-moz-box-shadow: 0px 3px 5px #474747;
	-webkit-box-shadow: 0px 3px 5px #474747;
	box-shadow: 0px 3px 5px #474747;
}
.circulo {
	margin-top: 40% !important;
	float: left!important;
	background-color: #ccc;
	display: block;
	width: 10px;
	height: 10px;
	margin: 3px! important;
	moz-border-radius: 75px;
	-webkit-border-radius: 75px;
	border-radius: 75px;
	margin: 5px auto;
	
}
.rojo{
	background-color: #cc0000;
}
.amarillo{
	background-color: #f1da36;
}
.verde {
	background-color: #8fc800;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		var idActivo = '';
		var indiceF = 0;
		var indiceII = 0;
		var indiceP = 0;
		$('#listaRiesgos').dataTable({
			bLengthChange : false,
			bProcessing : true,
			bFilter : false,
			serverSide : false,
			bRetrieve : true,
			scrollX : true,
			sAjaxSource : '/sgci/api/listarRiesgos',
			"columns" : [ {
				"data" : "codigo"
			}, {
				"data" : "nombre"
			}, {
				"data" : "amenaza"
			}, {
				"data" : "vulnerabilidad"
			}, {
				"data" : "c"
			}, {
				"data" : "i"
			}, {
				"data" : "d"
			}, {
				"data" : "cid"
			}, {
				"data" : "f",
				"render" : _SelF,
				"bSortable" : false
			}, {
				"data" : "ii",
				"render" : _SelII,
				"bSortable" : false
			}, {
				"data" : "valoracion",
				"render" : _Valoracion,
				"bSortable" : false
			}, {
				"data" : "riesgo"
			}, {
				"data" : "nombreRiesgo",
				"render" : _NombreRiesgo,
				"bSortable" : false
			}

			],
			"order" : [ [ 0, "asc" ] ]
		});
		
		function _SelF(data, type, full) {
			indiceF++;
			return '<select id="select_freq_'+indiceF+'" oninput="getValoracion(this)">'+
			'<option value="0" selected>Seleccione</option>'+
			'<option value="5">Casi Cierta</option>'+
			'<option value="4">Muy Probable</option>'+
			'<option value="3">Probable</option>'+
			'<option value="2">Poco Probable</option>'+
			'<option value="1">Rara Vez</option></select>';
		}
		
		function _SelII(data, type, full) {
			indiceII++;
			return '<select id="select_impa_'+indiceII+'" oninput="getValoracion(this)">'+
			'<option value="0" selected>Seleccione</option>'+
			'<option value="5">Casi Cierta</option>'+
			'<option value="4">Muy Probable</option>'+
			'<option value="3">Probable</option>'+
			'<option value="2">Poco Probable</option>'+
			'<option value="1">Rara Vez</option></select>';
		}
		
		function _Valoracion(data, type, full) {
			indiceP++;
			return '<div class="semaforo">'+
			'<div class="circulo" id="rojo_'+indiceP+'"></div>'+
		   ' <div class="circulo" id="amarillo_'+indiceP+'"></div>'+
		   ' <div class="circulo" id="verde_'+indiceP+'"></div>'+
			'</div>';
		}
		function _NombreRiesgo(data, type, full) {
			return '<input id="nombreRiesgo_'+indiceP+'"/>';
		}
		
	});
</script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container-fluid">
	<div class="row"></div>
	<div class="row">
		<div class="col-lg-12 col-md-12">
			<div class="card card-nav-tabs">
				<div class="card-header" data-background-color="red">
					<div class="nav-tabs-navigation">
						<div class="nav-tabs-wrapper">
							<ul class="nav nav-tabs" data-tabs="tabs">
								<li class="active"><a id="activo" data-toggle="tab"> <i class="material-icons">receipt</i>Plan de Tratamiento de Riesgos
										<div class="ripple-container"></div></a></li>
								<li class=""><a id="metrica" data-toggle="tab"> <i class="material-icons">code</i> Métrica
										<div class="ripple-container"></div></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div id="content-activo" class="col-md-12">
					<div class="col-md-12">
						<div class="col-md-6 text-center">
							<form class="navbar-form" role="search">
								<div class="form-group  is-empty">
									<input type="text" class="form-control" placeholder="Buscar por Categoría"> <span class="material-input"></span>
								</div>
								<button type="submit" class="btn btn-white btn-round btn-just-icon">
									<i class="material-icons">search</i>
									<div class="ripple-container"></div>
								</button>
							</form>
						</div>
						<div class="col-md-6 text-center">
							<form class="navbar-form " role="search">
								<div class="form-group  is-empty">
									<input type="text" class="form-control" placeholder="Buscar por Tipo"> <span class="material-input"></span>
								</div>
								<button type="submit" class="btn btn-white btn-round btn-just-icon">
									<i class="material-icons">search</i>
									<div class="ripple-container"></div>
								</button>
							</form>
						</div>
					</div>
					<div class="card">
						<div class="card-content table-responsive">
							<table class="table">
								
								<thead class="text-danger">
									<th>Código</th>
									<th>Nombre</th>
									<th>Amenaza</th>
									<th>Vulnerabilidad</th>
									<th>C</th>
									<th>I</th>
									<th>D</th>
									<th>Valor CID</th>
									<th>Tratamiento de Riesgo</th>
									<th>Control</th>
									<th>Actividad a Realizar</th>
									<th>Responsable</th>
									<th>Riesgo Residual</th>
								</thead>
								<tbody>

									<tr>
										<td>RI01</td>
										<td>Demora en realización de pruebas de POS</td>
										<td>Atención de incidencias tardías</td>
										<td>No existe conexión con MC Procesos</td>
										<td>5</td>
										<td>5</td>
										<td>4</td>
										<td id="cid_1">5</td>
										<td><input id=""></td>
										<td><input id=""></td>
										<td ><input id=""></td>
										<td><input id=""></td>
										<td><input id=""></td>
									</tr>
									
									<tr>
										<td>RI02</td>
										<td>Demora en la realización de pruebas</td>
										<td>Retraso de las pruebas</td>
										<td>No existe conexión con MC Procesos</td>
										<td>5</td>
										<td>4</td>
										<td>4</td>
										<td id="cid_1">4</td>
										<td><input id=""></td>
										<td><input id=""></td>
										<td ><input id=""></td>
										<td><input id=""></td>
										<td><input id=""></td>
									</tr>
									
									<tr>
										<td>RI03</td>
										<td>Demora en la relización de pruebas de ATM</td>
										<td>Atención de incidencias tardía</td>
										<td>No existe conexión con MC Procesos</td>
										<td>3</td>
										<td>5</td>
										<td>3</td>
										<td id="cid_1">4</td>
										<td><input id=""></td>
										<td><input id=""></td>
										<td ><input id=""></td>
										<td><input id=""></td>
										<td><input id=""></td>
									</tr>
									
									<tr>
										<td>RI04</td>
										<td>Falta de capacidad en los aplicativos Core</td>
										<td>Recursos de acceso desactualizados y centralizados en pocos analistas</td>
										<td>No todos los analistas conocen los recursos de acceso</td>
										<td>4</td>
										<td>4</td>
										<td>4</td>
										<td id="cid_1">4</td>
										<td><input id=""></td>
										<td><input id=""></td>
										<td ><input id=""></td>
										<td><input id=""></td>
										<td><input id=""></td>
									</tr>
									
									<tr>
										<td>RI05</td>
										<td>Resistencia al cambio en el uso de nuevas herramientas</td>
										<td>No permite el envío de correos automatizados para informar a otros anailista</td>
										<td>Resistencia al cambi</td>
										<td>4</td>
										<td>5</td>
										<td>3</td>
										<td id="cid_1">4</td>
										<td><input id=""></td>
										<td><input id=""></td>
										<td ><input id=""></td>
										<td><input id=""></td>
										<td><input id=""></td>
									</tr>
									</tbody>
							</table>

						</div>
					</div>
				</div>

				<div id="content-metrica" class="col-md-12">
					<div class="tab-content">
						<div class="tab-pane active" id="ptr">
							<div class="card">
								<img src="${prop['config.url.resources.base']}/images/metrica.png" class="img-responsive" style="margin-left: auto; margin-right: auto; display: block; width: 800px;">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
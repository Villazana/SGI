<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<nav class="navbar navbar-transparent navbar-absolute">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse">
				<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<!--a class="navbar-brand" href="#">Profile</a-->
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#pablo" class="dropdown-toggle" data-toggle="dropdown"> <i class="material-icons">dashboard</i>
						<p class="hidden-lg hidden-md">Dashboard</p>
				</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="material-icons">notifications</i> <span class="notification">5</span>
						<p class="hidden-lg hidden-md">Notifications</p>
				</a>
					<ul class="dropdown-menu">
						<li><a href="#">Notificación 1</a></li>
						<li><a href="#">Notificación 2</a></li>
						<li><a href="#">Notificación 3</a></li>
						<li><a href="#">Notificación 4</a></li>
						<li><a href="#">Notificación 5</a></li>
					</ul></li>
				<li><a href="#pablo" class="dropdown-toggle" data-toggle="dropdown"> <i class="material-icons">person</i>
						<p class="hidden-lg hidden-md">Profile</p>
				</a></li>
			</ul>
			<h3>
				<b>Calidad de Soluciones</b>
			</h3>
			<div id="fecha"></div>
			<div id="reloj"></div>
		</div>
	</div>
</nav>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="sidebar" data-color="red">
	<div class="logo">
		<a class="simple-text"> <img src="${prop['config.url.resources.base']}/images/bn.png" class="img-responsive" width="180px" style="margin-left: 26px;">
		</a>
	</div>
	<div class="sidebar-wrapper">
		<ul class="nav">
			<li ><a href="${prop['config.url.base']}/ingresarActivos"> <i class="material-icons">dashboard</i>
					<p>Ingreso de Activos</p>
			</a></li>
			<li><a href="${prop['config.url.base']}/modificarActivos"> <i class="material-icons">dashboard</i>
					<p>Identificación de Amenzas y Vulnerabilidades</p>
			</a></li>
			<li><a href="${prop['config.url.base']}/asignarRiesgo"> <i class="material-icons">content_paste</i>
					<p>Evaluación de Riesgos</p>
			</a></li>
			<li><a href="${prop['config.url.base']}/ptr"> <i class="material-icons">content_paste</i>
					<p>Plan de Tratamiento de Riesgos</p>
			</a></li>
			<li><a> <i class="material-icons">person</i>
					<p>Cerrar Sesión</p>
			</a></li>
		</ul>
	</div>
</div>
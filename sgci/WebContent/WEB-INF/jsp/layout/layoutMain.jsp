<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="${prop['config.url.resources.base']}/assets/img/apple-icon.png" />
<link rel="icon" type="image/png" href="${prop['config.url.resources.base']}/assets/img/favicon.png" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title><tiles:insertAttribute name="title" /></title>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />
<!-- Bootstrap core CSS     -->
<link href="${prop['config.url.resources.base']}/assets/css/bootstrap.min.css" rel="stylesheet" />
<!--  Material Dashboard CSS    -->
<link href="${prop['config.url.resources.base']}/assets/css/material-dashboard.css" rel="stylesheet" />
<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="${prop['config.url.resources.base']}/assets/css/demo.css" rel="stylesheet" />
<!--     Fonts and icons     -->
<link href="${prop['config.url.resources.base']}/assets/fonts/googlefonts.min.css" rel="stylesheet" type='text/css'>
<script src="${prop['config.url.resources.base']}/assets/js/jquery-3.1.0.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="${prop['config.url.resources.base']}/assets/datatables/datatables.min.css"/>
<script type="text/javascript" src="${prop['config.url.resources.base']}/assets/datatables/datatables.min.js"></script>
</head>
<body>
	<div class="wrapper">
		<tiles:insertAttribute name="menu"/>
	    <div class="main-panel">
	    	<tiles:insertAttribute name="panel"/>
	        <div class="content">
	            <tiles:insertAttribute name="content"/>
	        </div>
	    </div>
	</div>
	<div class="modal fade" id="modalAmenaza" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Agregar Amenaza</h4>
				</div>
				<form action="/sgci/api/grabarAmenaza" id="formularioAmenaza" method="post">
				<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Amenaza</label> <input type="text" class="form-control" name="descripcion" id="descripcion" required="required">
								</div>
							</div>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-success pull-right" id="butonGrabarAmenaza">Grabar</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalVulnerabilidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Agregar Vulneravilidad</h4>
				</div>
				<form action="/sgci/api/grabarVulnerablididad" id="formularioVulnerablididad" method="post">
				<div class="modal-body">	
					<div class="row">
						<div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label">Vulnerablididad</label> <input type="text" class="form-control" name="descripcion" id="descripcion" required="required">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-success pull-right" id="butonGrabarVulnerablididad">Grabar</button>
				</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalAmenazaVulnerabilida" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Agregar Amenaza y Vulneravilidad</h4>
				</div>
				<form action="/sgci/api/grabarAmenazaVulnerablididad" id="formularioAmenazaVulnerablididad" method="post">
				<div class="modal-body">	
					<div class="row">
						<div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label">Amenaza</label> <input type="text" class="form-control" name="amenaza" id="amenaza" required="required">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group label-floating">
								<label class="control-label">Vulnerablididad</label> <input type="text" class="form-control" name="vulnerabildad" id="vulnerabildad" required="required">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-success pull-right" id="butonGrabarAmenazaVulnerablididad">Grabar</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</body>
<!--   Core JS Files   -->

<script src="${prop['config.url.resources.base']}/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${prop['config.url.resources.base']}/assets/js/material.min.js" type="text/javascript"></script>
<!--  Notifications Plugin    -->
<script src="${prop['config.url.resources.base']}/assets/js/bootstrap-notify.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="${prop['config.url.resources.base']}/assets/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="${prop['config.url.resources.base']}/assets/js/demo.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#content-metrica").hide();
		$("#content-activo").show();
		$("#metrica").click(function() {
			$("#content-metrica").show();
			$("#content-activo").hide();
		});
		$("#activo").click(function() {
			$("#content-metrica").hide();
			$("#content-activo").show();
		});
	});
</script>
<script>
	function getValoracion(node) {
		var i = node.parentNode.parentNode.rowIndex;
		console.log(i)
		var frequencia = document.getElementById("select_freq_" + i).value;
		var impacto = document.getElementById("select_impa_" + i).value;
		var v = parseInt(frequencia) * parseInt(impacto);
		$('#verde_'+ i).removeAttr("style")
		$('#amarillo_'+ i).removeAttr("style")
		$('#rojo_'+ i).removeAttr("style")
		if (v <= 6) {
			$('#verde_'+ i).removeAttr("style")
			$('#verde_'+ i).css("background-color", "#8fc800");
		} else if (v <= 16) {
			$('#amarillo_'+ i).css("background-color", "#f1da36");
		} else if (v <= 25) {
			$('#rojo_'+ i).css("background-color", "#cc0000");
		}
	}
</script>
<script>
	function getValoracionCID(node) {
		var i = node.parentNode.parentNode.rowIndex;
		var c = parseInt(document.getElementById("select_c_" + i).value);
		var id = parseInt(document.getElementById("select_i_" + i).value);
		var d = parseInt(document.getElementById("select_d_" + i).value);  
		var valoracionCID = parseInt((c+id+d)/3); 
		document.getElementById("cid_" + i).innerHTML = valoracionCID;
	}
</script>
<script type="text/javascript">
	var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo",
			"Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
			"Diciembre");
	var diasSemana = new Array("Domingo", "Lunes", "Martes", "Miércoles",
			"Jueves", "Viernes", "Sábado");
	var f = new Date();
	document.getElementById('fecha').innerHTML = diasSemana[f.getDay()] + " "
			+ f.getDate() + " de " + meses[f.getMonth()] + " de "
			+ f.getFullYear();
</script>
<script type="text/javascript">
	function startTime() {
		today = new Date();
		h = today.getHours();
		m = today.getMinutes();
		s = today.getSeconds();
		m = checkTime(m);
		s = checkTime(s);
		document.getElementById('reloj').innerHTML = h + ":" + m + ":" + s;
		t = setTimeout('startTime()', 500);
	}
	function checkTime(i) {
		if (i < 10) {
			i = "0" + i;
		}
		return i;
	}
	window.onload = function() {
		startTime();
	}
</script>
</html>



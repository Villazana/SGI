package com.banco.nacion.calidad.bean;

public class EvaluacionRiesgoBean {

	private String codigo;
	private String nombre;
	private String amenaza;
	private String vulnerabilidad;
	private String c;
	private String i;
	private String d;
	private String cid;
	private String f;
	private String ii;
	private String valoracion;
	private String riesgo;
	private String nombreRiesgo;
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAmenaza() {
		return amenaza;
	}
	public void setAmenaza(String amenaza) {
		this.amenaza = amenaza;
	}
	public String getVulnerabilidad() {
		return vulnerabilidad;
	}
	public void setVulnerabilidad(String vulnerabilidad) {
		this.vulnerabilidad = vulnerabilidad;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public String getI() {
		return i;
	}
	public void setI(String i) {
		this.i = i;
	}
	public String getD() {
		return d;
	}
	public void setD(String d) {
		this.d = d;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getF() {
		return f;
	}
	public void setF(String f) {
		this.f = f;
	}
	public String getIi() {
		return ii;
	}
	public void setIi(String ii) {
		this.ii = ii;
	}
	public String getValoracion() {
		return valoracion;
	}
	public void setValoracion(String valoracion) {
		this.valoracion = valoracion;
	}
	public String getRiesgo() {
		return riesgo;
	}
	public void setRiesgo(String riesgo) {
		this.riesgo = riesgo;
	}
	public String getNombreRiesgo() {
		return nombreRiesgo;
	}
	public void setNombreRiesgo(String nombreRiesgo) {
		this.nombreRiesgo = nombreRiesgo;
	}
	
	
}

package com.banco.nacion.calidad.bean;

import java.io.Serializable;

public class AmenazaBean implements Serializable{

	private static final long serialVersionUID = -4554929580930824068L;
	
	private String codigo;
	private String codigoActivo;
	private String descripcion;
	
	public AmenazaBean() {
		super();
	}
	
	public AmenazaBean(String codigo, String codigoActivo, String descripcion) {
		super();
		this.codigo = codigo;
		this.codigoActivo = codigoActivo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodigoActivo() {
		return codigoActivo;
	}
	public void setCodigoActivo(String codigoActivo) {
		this.codigoActivo = codigoActivo;
	}
	
}

package com.banco.nacion.calidad.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.banco.nacion.calidad.bean.ActivoBean;
import com.banco.nacion.calidad.bean.AmenazaVulnerablidadBean;
import com.banco.nacion.calidad.bean.GenericBean;
import com.banco.nacion.calidad.service.ActivoService;
import com.banco.nacion.calidad.util.Response;

@RestController
@RequestMapping("api")
public class ActivosController {

	@Autowired
	ActivoService activoService;

	@RequestMapping(value = "grabarActivo", method = RequestMethod.POST)
	public @ResponseBody Response grabarActivo(@Valid ActivoBean activoBean, HttpServletRequest request) {
		return activoService.grabarActivo(activoBean,request);
	}

	@RequestMapping(value = "modificarActivo", method = RequestMethod.POST)
	public @ResponseBody Response modificarActivo(@Validated ActivoBean activoBean, HttpServletRequest request) {
		return activoService.modificarActivo(activoBean,request);
	}

	@RequestMapping(value = "listarActivos", method = RequestMethod.GET)
	public @ResponseBody GenericBean listarActivos(HttpServletRequest request) {
		return activoService.listarActivos(request);
	}
	
	@RequestMapping(value = "listarRiesgos", method = RequestMethod.GET)
	public @ResponseBody GenericBean listarRiesgos(HttpServletRequest request) {
		return activoService.listarRiesgos(request);
	}
	
	@RequestMapping(value = "grabarAmenazaVulnerablididad", method = RequestMethod.POST)
	public @ResponseBody Response grabarAmenazaVulnerablididad(@Valid AmenazaVulnerablidadBean amenazaVulnerablidadBean, HttpServletRequest request) {
		return activoService.grabarAmenazaVulnerablididad(amenazaVulnerablidadBean,request);
	}
}

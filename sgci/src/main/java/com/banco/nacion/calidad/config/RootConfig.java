package com.banco.nacion.calidad.config;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@ComponentScan(basePackages = "com.banco.nacion")
public class RootConfig {
	
	@Value(value = "file:/${propertiesBase}/configBase.properties")
	Resource configBase;
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setMessageConverters(messageConverters());
		return restTemplate;
	}

	@Bean
	public MappingJackson2HttpMessageConverter jsonHttpMessageConverter() {
		MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
		jsonHttpMessageConverter.setPrefixJson(false);
		jsonHttpMessageConverter.setSupportedMediaTypes(Arrays
				.asList(MediaType.APPLICATION_JSON));
		jsonHttpMessageConverter.setObjectMapper(objectMapper);
		return jsonHttpMessageConverter;
	}

	public List<HttpMessageConverter<?>> messageConverters() {

		List<MediaType> mediatypes = new ArrayList<MediaType>();
		mediatypes.add(new MediaType("text", "plain", StandardCharsets.UTF_8));
		mediatypes.add(new MediaType("*", "*", StandardCharsets.UTF_8));

		StringHttpMessageConverter stringHttpConverter = new StringHttpMessageConverter();
		stringHttpConverter.setSupportedMediaTypes(mediatypes);

		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(stringHttpConverter);
		messageConverters.add(jsonHttpMessageConverter());

		return messageConverters;
	}
	
	@Bean
	public PropertiesFactoryBean properties() {
		PropertiesFactoryBean propiedades = new PropertiesFactoryBean();
		propiedades.setLocations(configBase);
		return propiedades;
	}

	@Scope(value = "singleton")
	@Bean(name = "propiedad", initMethod = "init")
	Propiedad propiedad() {
		return new Propiedad();
	}
}

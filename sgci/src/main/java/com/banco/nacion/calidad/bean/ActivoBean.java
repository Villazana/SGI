package com.banco.nacion.calidad.bean;

import java.io.Serializable;
import java.util.List;

public class ActivoBean implements Serializable{

	private static final long serialVersionUID = 2011956370978011771L;
	
	private String id;
	private String nombre;
	private String descripcion;
	private String propietario;
	private String area;
	private String tipo;
	private String categoria;
	private String clasificacion;
	private String ubicacion;
	private String confidencialidad;
	private String integridad;
	private String disponibilidad;
	private List<AmenazaBean> listaAmenazas;
	private List<VulnerabilidadBean> listaVulnerabilidades;

	
	public ActivoBean() {
		super();
	}

	public ActivoBean(String id, String nombre, String descripcion, String propietario, String area, String tipo,
			String categoria, String clasificacion, String ubicacion, String confidencialidad, String integridad,
			String disponibilidad) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.propietario = propietario;
		this.area = area;
		this.tipo = tipo;
		this.categoria = categoria;
		this.clasificacion = clasificacion;
		this.ubicacion = ubicacion;
		this.confidencialidad = confidencialidad;
		this.integridad = integridad;
		this.disponibilidad = disponibilidad;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	private String valoracion;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getValoracion() {
		return valoracion;
	}

	public void setValoracion(String valoracion) {
		this.valoracion = valoracion;
	}

	public List<AmenazaBean> getListaAmenazas() {
		return listaAmenazas;
	}

	public void setListaAmenazas(List<AmenazaBean> listaAmenazas) {
		this.listaAmenazas = listaAmenazas;
	}

	public List<VulnerabilidadBean> getListaVulnerabilidades() {
		return listaVulnerabilidades;
	}

	public void setListaVulnerabilidades(List<VulnerabilidadBean> listaVulnerabilidades) {
		this.listaVulnerabilidades = listaVulnerabilidades;
	}

	public String getConfidencialidad() {
		return confidencialidad;
	}

	public void setConfidencialidad(String confidencialidad) {
		this.confidencialidad = confidencialidad;
	}

	public String getIntegridad() {
		return integridad;
	}

	public void setIntegridad(String integridad) {
		this.integridad = integridad;
	}

	public String getDisponibilidad() {
		return disponibilidad;
	}

	public void setDisponibilidad(String disponibilidad) {
		this.disponibilidad = disponibilidad;
	}

}

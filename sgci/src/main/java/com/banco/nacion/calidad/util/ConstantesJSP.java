package com.banco.nacion.calidad.util;

public class ConstantesJSP {

	public static final String PAGINA_ACTIVO_INGRESAR = "activo.ingreso";
	public static final String PAGINA_ACTIVO_MODIFICAR = "activo.modificar";
	public static final String PAGINA_RIESGO_ASIGNAR = "riesgo.asignar";
	public static final String PAGINA_TRATAMIENTO_RIESGO = "plan.tratamiento.riesgo";

}

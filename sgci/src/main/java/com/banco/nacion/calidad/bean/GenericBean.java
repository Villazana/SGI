package com.banco.nacion.calidad.bean;

import java.io.Serializable;

public class GenericBean implements Serializable{

	private static final long serialVersionUID = -4697673054541133840L;
	
	public Object data;

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}

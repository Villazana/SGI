package com.banco.nacion.calidad.util;

import java.util.ArrayList;
import java.util.List;

import com.banco.nacion.calidad.bean.ActivoBean;
import com.banco.nacion.calidad.bean.AmenazaBean;
import com.banco.nacion.calidad.bean.AmenazaVulnerablidadBean;
import com.banco.nacion.calidad.bean.VulnerabilidadBean;

public class Dummys {

	public static List<ActivoBean> dummyActivosRegistrados() {
		List<ActivoBean> listaActivos = new ArrayList<>();
		ActivoBean act1 = new ActivoBean("RP01","Aire acondicionado","Aire acondicionado","Martín Revilla","Calidad de Soluciones","Servicios","Inmueble","Público","Piso 11","2","2","2");
		ActivoBean act2 = new ActivoBean("RP02","Cobertores","Cobertores","Martín Revilla","Calidad de Soluciones","Servicios","Inmueble","Público","Piso 11","3","3","3");
		ActivoBean act3 = new ActivoBean("RP03","Nombrado","Nombrado","Martín Revilla","Calidad de Soluciones","RRHH","Personal","Uso Interno","Piso 11/4","3","4","3");
		ActivoBean act4 = new ActivoBean("RP04","Locador","Locador","Martín Revilla","Calidad de Soluciones","RRHH","Personal","Uso Interno","Piso 11/4","3","3","3");
		listaActivos.add(act1);
		listaActivos.add(act2);
		listaActivos.add(act3);
		listaActivos.add(act4);
		return listaActivos;
	}
	
	@Deprecated
	public static List<AmenazaBean> dummyAmenazas(){
		List<AmenazaBean> listaAmenaza = new ArrayList<>();
		AmenazaBean ame1 = new AmenazaBean("AM01","","A temperatura alta, mucho calor");
		AmenazaBean ame2 = new AmenazaBean("AM02","","No se puede regular para bajar temperatura");
		AmenazaBean ame3 = new AmenazaBean("AM03","","Produce reflejo molesto en las pantallas");
		listaAmenaza.add(ame1);
		listaAmenaza.add(ame2);
		listaAmenaza.add(ame3);
		return listaAmenaza;
	}
	
	@Deprecated
	public static List<VulnerabilidadBean> dummyVulnerabilidades(){
		List<VulnerabilidadBean> listaVul = new ArrayList<>();
		VulnerabilidadBean vul1 = new VulnerabilidadBean("VU01","","Aire Acondicionado uniforme en el edificio");
		VulnerabilidadBean vul2 = new VulnerabilidadBean("VU02","","Falta de cortinas y/o persianas");
		VulnerabilidadBean vul3 = new VulnerabilidadBean("VU03","","Asistencia del personal emfermo");
		listaVul.add(vul1);
		listaVul.add(vul2);
		listaVul.add(vul3);
		return listaVul;
	}
	
	
	
	public static List<AmenazaVulnerablidadBean> dummyAmenazasVulnerabilidades(){
		List<AmenazaVulnerablidadBean> lista = new ArrayList<>();
		AmenazaVulnerablidadBean obj1 = new AmenazaVulnerablidadBean("RP01","AM01","A temperatura alta, mucho calor","VU01","Aire Acondicionado uniforme en el edificio");
		lista.add(obj1);
		return lista;
	}
}

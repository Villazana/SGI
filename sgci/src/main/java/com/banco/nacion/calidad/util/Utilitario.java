package com.banco.nacion.calidad.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;
import org.springframework.web.util.WebUtils;

public class Utilitario {

	public static final String CODIGO_ACTIVO_NOMEMCLATURA = "RP";
	public static final Integer CODIGO_ACTIVO_INICIO = 5;
	public static final String CODIGO_AMENAZA_NOMEMCLATURA = "AM";
	public static final Integer CODIGO_AMENAZA_INICIO = 4;
	public static final String CODIGO_VULNERABLE_NOMEMCLATURA = "AM";
	public static final Integer CODIGO_VULNERABLE_INICIO = 4;
	public static final String ZERO = "0";
	
	public static String codigoActivo(HttpServletRequest request){
		String codigo = null;
		StringBuilder initCodigo = new StringBuilder();
		Integer numero = null;
		if(StringUtils.isEmpty(WebUtils.getSessionAttribute(request, "codigoActivo"))){
			initCodigo.append(CODIGO_ACTIVO_NOMEMCLATURA).append(ZERO).append(CODIGO_ACTIVO_INICIO);
			numero = CODIGO_ACTIVO_INICIO;
		}else{
			numero = (Integer) WebUtils.getSessionAttribute(request, "codigoActivo");
			initCodigo.append(CODIGO_ACTIVO_NOMEMCLATURA);
			if(numero < 10){initCodigo.append(ZERO);}
			initCodigo.append(numero);
		}
		WebUtils.setSessionAttribute(request, "codigoActivo", ++numero);
		codigo = initCodigo.toString();
		return codigo;
	}

	public static String codigoAmenaza(HttpServletRequest request) {
		String codigo = null;
		StringBuilder initCodigo = new StringBuilder();
		Integer numero = null;
		if(StringUtils.isEmpty(WebUtils.getSessionAttribute(request, "codigoAmenaza"))){
			initCodigo.append(CODIGO_AMENAZA_NOMEMCLATURA).append(ZERO).append(CODIGO_AMENAZA_INICIO);
			numero = CODIGO_AMENAZA_INICIO;
		}else{
			numero = (Integer) WebUtils.getSessionAttribute(request, "codigoAmenaza");
			initCodigo.append(CODIGO_AMENAZA_NOMEMCLATURA);
			if(numero < 10){initCodigo.append(ZERO);}
			initCodigo.append(numero);
		}
		WebUtils.setSessionAttribute(request, "codigoAmenaza", ++numero);
		codigo = initCodigo.toString();
		return codigo;
	}

	public static String codigoVulnerable(HttpServletRequest request) {
		String codigo = null;
		StringBuilder initCodigo = new StringBuilder();
		Integer numero = null;
		if(StringUtils.isEmpty(WebUtils.getSessionAttribute(request, "codigoVulnerable"))){
			initCodigo.append(CODIGO_VULNERABLE_NOMEMCLATURA).append(ZERO).append(CODIGO_VULNERABLE_INICIO);
			numero = CODIGO_VULNERABLE_INICIO;
		}else{
			numero = (Integer) WebUtils.getSessionAttribute(request, "codigoVulnerable");
			initCodigo.append(CODIGO_VULNERABLE_NOMEMCLATURA);
			if(numero < 10){initCodigo.append(ZERO);}
			initCodigo.append(numero);
		}
		WebUtils.setSessionAttribute(request, "codigoVulnerable", ++numero);
		codigo = initCodigo.toString();
		return codigo;
	}

	public static String calcularCID(String confidencialidad, String integridad, String disponibilidad) {
		
		int c= Integer.parseInt(confidencialidad);
		int id= Integer.parseInt(integridad);
		int d = Integer.parseInt(disponibilidad);
		int promedio = Integer.valueOf((c+id+d)/3);
	
		return String.valueOf(promedio);
		
	}

}

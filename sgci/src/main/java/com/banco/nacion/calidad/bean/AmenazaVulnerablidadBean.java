package com.banco.nacion.calidad.bean;

public class AmenazaVulnerablidadBean {

	private String codigoActivo;
	private String codigoAmenaza;
	private String amenaza;
	private String codigoVulnerabildad;
	private String vulnerabildad;
	
	
	
	public AmenazaVulnerablidadBean() {
		super();
	}

	public AmenazaVulnerablidadBean(String codigoActivo, String codigoAmenaza, String amenaza,
			String codigoVulnerabildad, String vulnerabildad) {
		super();
		this.codigoActivo = codigoActivo;
		this.codigoAmenaza = codigoAmenaza;
		this.amenaza = amenaza;
		this.codigoVulnerabildad = codigoVulnerabildad;
		this.vulnerabildad = vulnerabildad;
	}
	
	public String getCodigoActivo() {
		return codigoActivo;
	}
	public void setCodigoActivo(String codigoActivo) {
		this.codigoActivo = codigoActivo;
	}
	public String getCodigoAmenaza() {
		return codigoAmenaza;
	}
	public void setCodigoAmenaza(String codigoAmenaza) {
		this.codigoAmenaza = codigoAmenaza;
	}
	public String getAmenaza() {
		return amenaza;
	}
	public void setAmenaza(String amenaza) {
		this.amenaza = amenaza;
	}
	public String getCodigoVulnerabildad() {
		return codigoVulnerabildad;
	}
	public void setCodigoVulnerabildad(String codigoVulnerabildad) {
		this.codigoVulnerabildad = codigoVulnerabildad;
	}
	public String getVulnerabildad() {
		return vulnerabildad;
	}
	public void setVulnerabildad(String vulnerabildad) {
		this.vulnerabildad = vulnerabildad;
	}
	
	
}

package com.banco.nacion.calidad.bean;

import java.io.Serializable;

public class VulnerabilidadBean implements Serializable{
	
	private static final long serialVersionUID = -1483730492241881911L;
	
	private String codigo;
	private String codigoActivo;
	private String descripcion;
	
	
	
	public VulnerabilidadBean() {
		super();
	}
	
	public VulnerabilidadBean(String codigo, String codigoActivo, String descripcion) {
		super();
		this.codigo = codigo;
		this.codigoActivo = codigoActivo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCodigoActivo() {
		return codigoActivo;
	}
	public void setCodigoActivo(String codigoActivo) {
		this.codigoActivo = codigoActivo;
	}

}

package com.banco.nacion.calidad.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.banco.nacion.calidad.util.ConstantesJSP;

@Controller
@RequestMapping("/")
public class FormController {

	@RequestMapping(value ={"","/", "ingresarActivos"}, method = RequestMethod.GET)
	public String ingresoActivos(ModelMap model) {
		return ConstantesJSP.PAGINA_ACTIVO_INGRESAR;
	}

	@RequestMapping(value = "modificarActivos", method = RequestMethod.GET)
	public String modificarActivos(ModelMap model) {
		return ConstantesJSP.PAGINA_ACTIVO_MODIFICAR;
	}

	@RequestMapping(value = "asignarRiesgo", method = RequestMethod.GET)
	public String asignarRiesgo(ModelMap model) {
		return ConstantesJSP.PAGINA_RIESGO_ASIGNAR;
	}
	
	@RequestMapping(value = "ptr", method = RequestMethod.GET)
	public String planTratamientoRiesgo(ModelMap model) {
		return ConstantesJSP.PAGINA_TRATAMIENTO_RIESGO;
	}
}

package com.banco.nacion.calidad.service;

import javax.servlet.http.HttpServletRequest;

import com.banco.nacion.calidad.bean.ActivoBean;
import com.banco.nacion.calidad.bean.AmenazaVulnerablidadBean;
import com.banco.nacion.calidad.bean.GenericBean;
import com.banco.nacion.calidad.util.Response;

public interface ActivoService {

	Response grabarActivo(ActivoBean activoBean,HttpServletRequest request);

	Response modificarActivo(ActivoBean activoBean,HttpServletRequest request);

	GenericBean listarActivos(HttpServletRequest request);

	Response grabarAmenazaVulnerablididad(AmenazaVulnerablidadBean amenazaVulnerablidadBean, HttpServletRequest request);

	GenericBean listarRiesgos(HttpServletRequest request);
}

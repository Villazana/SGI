package com.banco.nacion.calidad.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.util.WebUtils;

import com.banco.nacion.calidad.bean.ActivoBean;
import com.banco.nacion.calidad.bean.AmenazaVulnerablidadBean;
import com.banco.nacion.calidad.bean.EvaluacionRiesgoBean;
import com.banco.nacion.calidad.bean.GenericBean;
import com.banco.nacion.calidad.service.ActivoService;
import com.banco.nacion.calidad.util.Dummys;
import com.banco.nacion.calidad.util.Response;
import com.banco.nacion.calidad.util.Utilitario;

@Service
public class ActivoServiceImpl implements ActivoService{

	@SuppressWarnings("unchecked")
	@Override
	public Response grabarActivo(ActivoBean activoBean,HttpServletRequest request) {
		List<ActivoBean> lista = null;
		List<ActivoBean> listaActivos = null;
		Response response = new Response();
		try {
			listaActivos = (List<ActivoBean>)WebUtils.getSessionAttribute(request, "listarActivos");
			if(StringUtils.isEmpty(listaActivos)){
				lista = new ArrayList<>();
				activoBean.setId(Utilitario.codigoActivo(request));
				lista.add(activoBean);
				WebUtils.setSessionAttribute(request, "listarActivos",lista);
			}else{
				activoBean.setId(Utilitario.codigoActivo(request));
				listaActivos.add(activoBean);
				WebUtils.setSessionAttribute(request, "listarActivos",listaActivos);
			}
			response.setEstado(1);
			response.setMensaje("Se registro correctamente");
		} catch (Exception e) {
			response.setEstado(0);
			response.setMensaje("No se registró");
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Response modificarActivo(ActivoBean activoBean,HttpServletRequest request) {
		List<ActivoBean> listaActivos = null;
		List<ActivoBean> lista = null;
		Response response = null;
		try {
			response = new Response();
			listaActivos = (List<ActivoBean>)WebUtils.getSessionAttribute(request, "listarActivos");
			if(!StringUtils.isEmpty(listaActivos)){
				for (ActivoBean activo : listaActivos) {
					if(activoBean.getId().equals(activo.getId())){
						ActivoBean modificado = new ActivoBean();
						modificado.setNombre(activo.getNombre());
						modificado.setDescripcion(activo.getDescripcion());
						modificado.setPropietario(activo.getPropietario());
						modificado.setTipo(activo.getTipo());
						modificado.setCategoria(activo.getCategoria());
						modificado.setValoracion(activo.getValoracion());
						lista.add(modificado);
					}else{
						lista = new ArrayList<>();
						lista.add(activo);
					}
				}
			}
			WebUtils.setSessionAttribute(request, "listarActivos",lista);
			response.setEstado(1);
			response.setMensaje("Se encontrarón registros");
		} catch (Exception e) {
			response = new Response();
			response.setEstado(0);
			response.setMensaje("No se encontro registros");
		}
		
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public GenericBean listarActivos(HttpServletRequest request) {
		GenericBean response = new GenericBean();
		List<ActivoBean> lista = new ArrayList<>();
		lista.addAll(Dummys.dummyActivosRegistrados());
		if(!StringUtils.isEmpty(WebUtils.getSessionAttribute(request,"listarActivos"))){
			lista.addAll((List<ActivoBean>)WebUtils.getSessionAttribute(request,"listarActivos"));
		}
		response.setData(lista);
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Response grabarAmenazaVulnerablididad(AmenazaVulnerablidadBean amenazaVulnerablidadBean, HttpServletRequest request) {
		List<AmenazaVulnerablidadBean> lista = null;
		List<AmenazaVulnerablidadBean> listaAmenazasVulnerables = null;
		Response response = new Response();
		try {
			listaAmenazasVulnerables = (List<AmenazaVulnerablidadBean>)WebUtils.getSessionAttribute(request, "listarAmenazasVulnerables");
			if(StringUtils.isEmpty(listaAmenazasVulnerables)){
				lista = new ArrayList<>();
				amenazaVulnerablidadBean.setCodigoAmenaza(Utilitario.codigoAmenaza(request));
				amenazaVulnerablidadBean.setCodigoVulnerabildad(Utilitario.codigoVulnerable(request));
				lista.add(amenazaVulnerablidadBean);
				WebUtils.setSessionAttribute(request, "listarAmenazasVulnerables",lista);
			}else{
				amenazaVulnerablidadBean.setCodigoAmenaza(Utilitario.codigoAmenaza(request));
				amenazaVulnerablidadBean.setCodigoVulnerabildad(Utilitario.codigoVulnerable(request));
				listaAmenazasVulnerables.add(amenazaVulnerablidadBean);
				WebUtils.setSessionAttribute(request, "listarAmenazasVulnerables",listaAmenazasVulnerables);
			}
			response.setEstado(1);
			response.setMensaje("Se registro correctamente");
		} catch (Exception e) {
			response.setEstado(0);
			response.setMensaje("No se registró");
		}
		return response;
	}

	@SuppressWarnings("unchecked")
	@Override
	public GenericBean listarRiesgos(HttpServletRequest request) {
		Integer indiceRiesgo = 0;
		GenericBean response = new GenericBean();
		List<ActivoBean> activos = new ArrayList<>();
		List<AmenazaVulnerablidadBean> registros = new ArrayList<>();
		List<EvaluacionRiesgoBean> evaluaciones = new ArrayList<>();
		activos.addAll(Dummys.dummyActivosRegistrados());
		registros.addAll(Dummys.dummyAmenazasVulnerabilidades());
		if(!StringUtils.isEmpty(WebUtils.getSessionAttribute(request,"listarActivos"))){
			activos.addAll((List<ActivoBean>)WebUtils.getSessionAttribute(request,"listarActivos"));
		}
		if(!StringUtils.isEmpty(WebUtils.getSessionAttribute(request,"listarAmenazasVulnerables"))){
			registros.addAll((List<AmenazaVulnerablidadBean>)WebUtils.getSessionAttribute(request,"listarAmenazasVulnerables"));
		}
		
		for (AmenazaVulnerablidadBean registro : registros) {
			String codigoActivo;
			EvaluacionRiesgoBean evaluacion = new EvaluacionRiesgoBean();
			codigoActivo = registro.getCodigoActivo();
			for (ActivoBean activo : activos) {
				if(codigoActivo.equals(activo.getId())){
					evaluacion.setCodigo(activo.getId());
					evaluacion.setNombre(activo.getNombre());
					evaluacion.setC(activo.getConfidencialidad());
					evaluacion.setI(activo.getIntegridad());
					evaluacion.setD(activo.getDisponibilidad());
					evaluacion.setCid(Utilitario.calcularCID(activo.getConfidencialidad(),activo.getIntegridad(),activo.getDisponibilidad()));
					evaluacion.setAmenaza(registro.getAmenaza());
					evaluacion.setVulnerabilidad(registro.getVulnerabildad());
					evaluacion.setVulnerabilidad(registro.getVulnerabildad());
					indiceRiesgo++;
					evaluacion.setRiesgo("RI-"+indiceRiesgo);
					evaluaciones.add(evaluacion);
				}
			}
		}
		
		response.setData(evaluaciones);
		return response;
	}

}

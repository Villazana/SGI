package com.banco.nacion.calidad.bean;

public class RiesgoBean {
	
	private String codigoRiesgo;
	private String nombreRiesgo;
	private String nivel;
	private String tratamiento;
	private String control;
	private String actividadRealizada;
	private String responsable;
	private String area;
	private String probabilidadResidual;
	private String riesgoResidual;
	
	public String getCodigoRiesgo() {
		return codigoRiesgo;
	}
	public void setCodigoRiesgo(String codigoRiesgo) {
		this.codigoRiesgo = codigoRiesgo;
	}
	public String getNombreRiesgo() {
		return nombreRiesgo;
	}
	public void setNombreRiesgo(String nombreRiesgo) {
		this.nombreRiesgo = nombreRiesgo;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public String getTratamiento() {
		return tratamiento;
	}
	public void setTratamiento(String tratamiento) {
		this.tratamiento = tratamiento;
	}
	public String getControl() {
		return control;
	}
	public void setControl(String control) {
		this.control = control;
	}
	public String getActividadRealizada() {
		return actividadRealizada;
	}
	public void setActividadRealizada(String actividadRealizada) {
		this.actividadRealizada = actividadRealizada;
	}
	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getProbabilidadResidual() {
		return probabilidadResidual;
	}
	public void setProbabilidadResidual(String probabilidadResidual) {
		this.probabilidadResidual = probabilidadResidual;
	}
	public String getRiesgoResidual() {
		return riesgoResidual;
	}
	public void setRiesgoResidual(String riesgoResidual) {
		this.riesgoResidual = riesgoResidual;
	}
	
}
